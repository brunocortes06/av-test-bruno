package com.avenuecode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.avenuecode.api.model.Image;
import com.avenuecode.api.model.Product;
import com.avenuecode.core.persistence.ProductRepository;

@SpringBootApplication
public class Application {
	
	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@Bean
	public CommandLineRunner setup(ProductRepository productRepository) {
		return (args) -> {
			
			Product macBookPro = new Product(1, "Macbook Pro", "Apple");
			macBookPro.addImage(new Image(1, "Box", "/images/base.jpg"));
			macBookPro.addImage(new Image(2, "Front", "/images/front.jpg"));
			macBookPro.addImage(new Image(3, "Side", "/images/side.jpg"));
			
			productRepository.save(macBookPro);
			productRepository.save(new Product(2, "iPhone", "Apple"));
			productRepository.save(new Product(3, "Monitor Retina", "Apple"));
			productRepository.save(new Product(4, "Nokia 331", "Nokia"));
			productRepository.save(new Product(5, "Golf TSI", "VW"));
			logger.info(Application.class + " All Data Created");
		};
	}
}
