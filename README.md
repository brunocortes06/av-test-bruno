### The project was created with Java 8 + maven ###

### Description ###
This project list products and sub products, with all the related data(images...) according to the given task description:

a) Get all products excluding relationships (child products, images)
b) Get all products including specified relationships (child product and/or images)
c) Same as 1 using specific product identity
d) Same as 2 using specific product identity
e) Get set of child products for specific product
f) Get set of images for specific product

### Test ###

mvn clean
mvn test


## Start Application ##

mvn clean
mvn install
mvn spring-boot:run


### REST API ###

* Get all products: 
	http://localhost:8080/products

* Get all products excluding relationships: 
	http://localhost:8080/products?exclude=true

* Get a specific product: 
	http://localhost:8080/products/{id}

* Get a specific product excluding relationships: 
	http://localhost:8080/products/{id}?exclude=true

* Get set of child products for specific product: 
	http://localhost:8080/products/{id}/subproducts

* Get set of images for specific product: 
	http://localhost:8080/products/{id}/imgs

* Get all images: http://localhost:8080/imgs

* POST - Add a product: 
	http://localhost:8080/products/

{
  "name": "name",
  "description": "description",
  "images" : [{"type": "imageType1", "path": "/home/type/image1"}, {"type": "imageType2", "path": "/home/type/image2"}],
  "subProducts" : [
  		{
  		  "name": "subProduct1",
		  "description": "subProduct"
  		}
  	]
}